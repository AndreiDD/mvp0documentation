# MVP0 API
  
There are just 3 REST API endpoints.
  
### 1. Verify that the server is alive  
  
#### GET https://blockchain.to.wtf/api/ping   
  
should respond with "pong"  

You can also view the status of the blockchain private network here: <a href="http://94.26.15.137:10027/">netstats</a>
  
### 2. Read from the Blockchain  
  
#### GET  /api/record/{UNIQUE_ID_OF_THE_RECORD}  
  
where the {UNIQUE_ID_OF_THE_RECORD} is an unique ID that you should have (or generate)  
  
- a 32 random mixed characters without spaces should be ok  
  
##### Example:  
  
~~~  
https://blockchain.to.wtf/api/record/a0b1c2d3e4f5g6h7i8  
~~~  
  
##### Response:  
  
~~~~  
{  
 "userID": "abldkjflsdakjfas", "timestamp": 123123123, "amount": 122.33123, "currency": "USD", "transactionID": "cxivjlkq2kjflaksfdj", "metadata": "xlkvjlkjiejwrlkfjsdlkjfsdalkjfasdlfjaksljf"}  
~~~~  

Here is an example in POSTMAN https://www.getpostman.com/downloads/

<img src="https://gitlab.com/AndreiDD/mvp0documentation/raw/master/getting_from_blockchain.png" alt="getting" />
  
### 3. Write on the Blockchain  
  
#### POST  /api/record  
  
~~~  
https://blockchain.to.wtf/api/record  
~~~  
  
with the payload  
  
~~~~  
{  
 "id":"a0b1c2d3e4f5g6h7i8", "payload": "ew0KICAidXNlcklEIjogImFibGRramZsc2Rha2pmYXMiLA0KICAidGltZXN0YW1wIjogMTIzMTIzMTIzLA0KICAiYW1vdW50IjogMTIyLjMzMTIzLA0KICAiY3VycmVuY3kiOiAiVVNEIiwNCiAgInRyYW5zYWN0aW9uSUQiOiAiY3hpdmpsa3Eya2pmbGFrc2ZkaiIsDQogICJtZXRhZGF0YSI6ICJ4bGt2amxramllandybGtmanNkbGtqZnNkYWxramZhc2RsZmpha3NsamYiDQp9"}  
~~~~  

<img src="https://gitlab.com/AndreiDD/mvp0documentation/raw/master/posting_to_blockchain.png" alt="posting" />


#### Parameters:

id: the unique generated ID that you have for generated especially for this record. Should be kept in your database as UNIQUE. It is used to retrieve data from the blockchain

payload: the base64 encoded version of your payload. 
> you can use [https://www.base64encode.net/](https://www.base64encode.net/) to test if you encode/decode correctly

Notes:
- keep it short (maximum 3mb is allowed, but it should be much less)
- this server encrypts it before sending it to the blockchain, and decrypts->decodes it on retrieving it.
  
##### Response:  
  
at this moment, **don't use the tx_hash for anything**. 
> later you can use it to check if the transaction got included in the blockchain or not,  
but for not disregard it   
~~~~  
{  
 "tx_hash": "0x50620dad8b86f145f4192e0d9a642daebcd099ec3d7573e2106d7e917ecc51ce"}  
~~~~
